﻿#include <iostream>
#include <cmath>

using namespace std;

class Vector
{
public:
	Vector(): x(0), y(0), z(0)
	{}
	Vector(double _x, double _y, double _z) : x(_x), y(_y), z(_z)
	{}
	void Modul() 
	{
		double vModul = sqrt(pow(x,2) + pow(y, 2) + pow(z, 2));
		cout<< "Length is " << vModul;
	}
	void Show() 
	{
		cout << '\n' << x << ' ' << y << ' ' << z;
	}
private:
	double x;
	double y;
	double z;
};


int main()
{
	Vector v(10,9,6);
	v.Modul();
	v.Show();
}

