﻿#include <iostream>
#include <ctime>
using namespace std;

int main()
{
    time_t now = time(0);
    struct tm timeinfo;
    localtime_s(&timeinfo, &now);
    int dayOfMonth = timeinfo.tm_mday;

    const int n = 3;
    int arr[n][n];

    int divisionRemain = dayOfMonth % n;

    for (int i = 0; i < n; i++) 
    {
        for (int j = 0; j < n; j++)
        {
            arr[i][j] = i + j;
            cout << arr[i][j];
        }
        cout << "\n";
    }

    cout << arr[divisionRemain][0] + arr[divisionRemain][1] + arr[divisionRemain][2];
}

